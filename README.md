# No Step on Snake

No Step on Snake is a group collaborated python-based application that implements web scraping and parsing on the Python's documentation web pages to create a
game revolving around the OS and Sys Python libraries utilizing Pygame.
No Step on Snake is a group collaborated python-based application that implements web scraping and parsing on the Python's documentation web pages to create an application revolving around the OS and Sys Python libraries utilizing Pygame.

## Dependencies

Requests
JSON
PYGAME_MENU
PYGAME

```bash
pip install beautifulsoup4
pip install requests
pip install pygame_menu
```
## Description
The application opens up a GUI menu that allows you to choose between searching libraries or  quitting.
![image](/uploads/7a692088f1d440197bb2e3fc4372c67d/image.png)

When the Search Library text box is selected it takes the user to another GUI interface called
'Function Finder'
![image](/uploads/e859c3cda6d284ac0a4bfc7c73a708a1/image.png)
Function Finder takes in the users information and searches the os and sys libraries from the python documentation from these URL's :
https://docs.python.org/3/library/os.html 
https://docs.python.org/3/library/sys.html 

When the user enters a function it must contain the library prefix such as 'os' or 'sys' Example: os.name or sys.version.

When the user presses the search button in the Function Finder it will run a webscraper on the html from the two libraries to search for the function.  If the function is found it will return the definiton of the function.
There are also examples that can be displayed if they are available.  At this point there are only 10 examples from each library available for demonstration purposes.

If the definiton is too lengthy to fit into the definiton display box then the user is able to scroll to read the whole definiton.



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
Cyber W@rr1iorz [TC, CR, AP, KA]
