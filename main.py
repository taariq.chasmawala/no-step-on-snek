from gui import program_display
from functions_dict import scrapedata_sys, scrapedata_os, os_examples_dictionary, sys_examples_dictionary
import pygame
import pygame_menu

# initiate pygame
# VS Code cannot recognize pygames' members and will display an error.
# the code will run with most errors involving pygame modules not having a specified member
pygame.init()

# constants
# win is the main surface for the program
win = pygame.display.set_mode((800, 600))
pygame.display.set_caption('Dis Game')
pygame.display.set_icon(pygame.image.load('nsos.jpg'))
font = pygame.font.SysFont('ariel', 30, True)

# this should call Kevin Ariaga's code to search theough the sys and os libraries
# this also calls Cass' code to get the definitions and examples
def main():
    program_display(scrapedata_os(), os_examples_dictionary(), scrapedata_sys(), sys_examples_dictionary())

main_menu = None
running = True

# Main menu function
def da_menu():
    global main_menu
    # create the menu
    main_menu = pygame_menu.Menu(300, 400, 'The Libraries',
                       theme=pygame_menu.themes.THEME_ORANGE)
    # add buttons for the main menu
    main_menu.add_button('Search Libraries', main)
    main_menu.add_button('Quit', pygame_menu.events.EXIT)
    # when the program starts the main menu will appear first on the window
    main_menu.mainloop(win)

# keeps the window from closing once it opens by keeping the menu open
da_menu()

if __name__ == "__main__":
    main()