'''
This program creates a GUI where you can look up system/os functions and get a definition and example of it.
'''
# Imports
import tkinter as tk
from functions_dict import scrapedata_sys, scrapedata_os, os_examples_dictionary, sys_examples_dictionary


# Displays GUI
def program_display(os_def, os_ex, sys_def, sys_ex):
    # Runs search for given function
    def search(entry):
        # Check if entry is in dictionary
        if entry in os_def:
            define = os_def[entry]
            # Check if entry has an example
            if entry in os_ex:
                sample = os_ex[entry]
            else:
                sample = 'No example available'
        elif entry in sys_def:
            define = sys_def[entry]
            # Check if entry has an example
            if entry in sys_ex:
                sample = sys_ex[entry]
            else:
                sample = 'No example available'
        else:
            # Not in OS or SYS libraries
            define = 'No definition available'
            sample = 'No example available'

        # Assign definition and example to output 
        definition['text'] = define 
        example['text'] = sample

    # Creating Root
    root = tk.Tk()
    root.title('Function Finder')

    # Creating window
    canvas = tk.Canvas(root, height=768, width=1024)
    canvas.pack()

    # Creating frames and labels
    search_Label = tk.Label(root, text='Enter function to search', font=(None, 15))
    search_Label.place(relx=0.05, rely=0.01)

    search_Frame = tk.Frame(root, bg='#BBBBBB')
    search_Frame.place(relx=0.5, rely=0.05, relwidth=0.9, relheight=0.1, anchor='n')

    def_Label = tk.Label(root, text='Definition', font=(None, 15))
    def_Label.place(relx=0.05, rely=0.16)

    def_Frame = tk.Frame(root, bg='#BBBBBB')
    def_Frame.place(relx=0.5, rely=0.20, relwidth=0.9, relheight=0.2, anchor='n')

    ex_Label = tk.Label(root, text='Example', font=(None, 15))
    ex_Label.place(relx=0.05, rely=0.41)

    ex_Frame = tk.Frame(root, bg='#BBBBBB')
    ex_Frame.place(relx=0.5, rely=0.45, relwidth=0.9, relheight=0.5, anchor='n')

    # Creating items for search frame
    entry = tk.Entry(search_Frame, font=(None, 15))
    entry.place(relx=0.025, rely=0.25, relwidth=0.75, relheight=0.50)

    button = tk.Button(search_Frame, text='Search', font=(None, 15), command=lambda: search(entry.get())) # Calls search function on button press
    button.place(relx=0.8, rely=0.25, relwidth=0.18, relheight=0.50)

    # Creating items for definition frame
    def_canvas = tk.Canvas(def_Frame, scrollregion=(0,0,0,400))
    def_canvas.place(relx=0.025, rely=0.1, relwidth=0.9, relheight=0.8)
    
    scroll = tk.Scrollbar(def_Frame, orient='vertical', command=def_canvas.yview)
    scroll.place(relx=0.925, rely=0.1, relwidth=.05, relheight=0.8)
    def_canvas.config(yscrollcommand=scroll.set)

    definition = tk.Label(def_canvas, justify='left', anchor='w', wraplength=650, font=(None, 13))
    def_canvas.create_window(0, 0, anchor='nw', window=definition)

    # Creating items for example frame
    example = tk.Label(ex_Frame, justify='left', anchor='nw', wraplength=650, font=(None, 13))
    example.place(relx=0.025, rely=0.05, relwidth=0.95, relheight=0.9)

    root.mainloop()

if __name__ == "__main__":
    program_display(scrapedata_os(), os_examples_dictionary(), scrapedata_sys(), sys_examples_dictionary())