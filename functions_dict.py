from bs4 import BeautifulSoup
import requests
import html
import json

# Clean list of all function names from href attribute
"""
total_function_list = []
for row in function_list_1:
    total_function_list.append(row.a['href'][1:])
for row in function_list_2:
    total_function_list.append(row.a['href'][1:]) 
"""

OS_LIB_URL = 'https://docs.python.org/3/library/os.html'
SYS_LIB_URL = 'https://docs.python.org/3/library/sys.html'
OS_EXAMPLES_FILENAME = "os_examples.txt"
SYS_EXAMPLES_FILENAME = "sys_examples.txt"

# create soup object 
def create_soup_obj(html_url):
    os_url = html_url
    html_text = requests.get(os_url).text
    return BeautifulSoup(html_text, 'html.parser')

# return dictionary of "dl tag" information
def function_def_dict(soup_object):
    function_list_1 = soup_object.find_all("dl", class_="function")
    function_list_2 = soup_object.find_all("dl", class_="data")
    function_dict = {}

    for row in function_list_1:
        key = row.find("dt").text[1:-2]
        for i in range(len(key)):
            if key[i] == "(":
                parentheses_index = i
                break
        key = key[:parentheses_index]
        desc = "".join(row.find("dd").text.strip().replace('\n', ' '))
        function_dict[key] = desc

    for row in function_list_2:
        key = row.find("dt").text[1:-2]
        desc = "".join(row.find("dd").text.strip().replace('\n', ' '))
        function_dict[key] = desc
    
    return function_dict

# return os functions from documentation
def scrapedata_os():
    return function_def_dict(create_soup_obj(OS_LIB_URL))

# return sys functions from documentation
def scrapedata_sys():
    return function_def_dict(create_soup_obj(SYS_LIB_URL))

# parse file formatted as dictionary and use json to parse it. return dictionary object
def json_parse_examples(file_name):
    with open(file_name) as f: 
        data = f.read() 

    # reconstructing the data as a dictionary 
    dictionary = json.loads(data)
    return dictionary

# return os examples dictionary
def os_examples_dictionary():
    return json_parse_examples(OS_EXAMPLES_FILENAME)

# return sys examples dictionary
def sys_examples_dictionary():
    return json_parse_examples(SYS_EXAMPLES_FILENAME)

def main():
    os_dict = function_def_dict(create_soup_obj(OS_LIB_URL))
    sys_dict = function_def_dict(create_soup_obj(SYS_LIB_URL))
    print(os_dict)
    print(sys_dict)

if __name__ == "__main__":
    main()